/*
 * while.c   循环显示
 *
 * Copyright (C) 2014, 2018   Caipenghui   蔡鹏辉   All Rights Reserved.
 *
 * Email: <Caipenghui_c@163.com>
 *
 *   ∧_∧::
 *  (´･ω･`)::
 *  /⌒　　⌒)::
 * /へ_＿  / /::
 * (＿＼＼  ﾐ)/::
 * ｜ `-イ::
 * /ｙ　 )::
 * /／  ／::
 * ／　／::
 * (　く:::
 * |＼ ヽ:::
 */

#inclde <stdio.h>

int main(void)
{
        int a = 9, b = 2;
        while (a > b) {
                printf("-----\n");
        }
        return 0;
}       
